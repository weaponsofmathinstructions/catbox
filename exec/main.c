#include <stdio.h>

#include "edebug.h"
#include "server.h"

int
main( int argc, char **argv )
{
    const char *port = argc == 2 ? argv[1] : "8000";
    struct server   srv;

    check( !srv_constr( &srv ), "Could not initialize server" );
    check( srv_bind( &srv, port ), "Could not bind to port" );

    check( !srv_start( &srv ), "Server did not run propperly" );

    /* This is never reached. TODO: call srv_stop on a signal */
    srv_destr( &srv );

    return 0;
error:
    srv_destr( &srv );
    return 1;
}
