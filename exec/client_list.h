#pragma once
#include <pthread.h>
#include <stddef.h>

#include "client.h"

/**
 * Thread safe container for client connections.
 */
struct client_list {
    struct client  *c;
    size_t          n;
    size_t          max;
    pthread_mutex_t mutex;
};

/**
 * Constructor.
 * Creates a array of clients.
 * @param max   Wanted capacity in the list.
 * @return 0 on success, -1 on failure.
 */
int  cl_constr( struct client_list *cl, size_t max );
/**
 * Destructor.
 * Calls the destructor of all members and frees memory.
 */
void cl_destr( struct client_list *cl );

/**
 * Pushes the client to the end of the array and assumes ownership.
 * @return 0 on success, -1 on failure (not enough space).
 */
int  cl_push( struct client_list *cl, struct client *cli );
/**
 * Removes the first client in the array and assumes you take ownership
 * of it.
 * @return 0 on success, -1 on failure (empty list).
 */
int  cl_shift( struct client_list *cl, struct client *cli );

static inline void
cl_lock( struct client_list *cl )
{
    pthread_mutex_lock( &cl->mutex );
}
static inline void
cl_unlock( struct client_list *cl )
{
    pthread_mutex_unlock( &cl->mutex );
}
