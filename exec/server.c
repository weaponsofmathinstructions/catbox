/* getaddrinfo and friends are not part of c99 */
#define _POSIX_C_SOURCE 200112L

#include <arpa/inet.h>
#include <dlfcn.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>

#include "catbox.h"
#include "client.h"
#include "edebug.h"
#include "request_priv.h"
#include "server.h"
#include "utility.h"

struct {
    const char *path;
    const char *filename;
} config[] = {
    { "",           "fileserv.so" },    /* First is default handler */
    { "/relay",     "relay.so" },
    { "/makepng",   "makepng.so" },
};

/*
 * Static declarations
 */
int
listen_port( const char *port );

void
srv_handle( const struct server *srv, struct client *cli );

static inline
const char *
get_name( char *dst, size_t dst_len, struct sockaddr_storage *ss );


/*
 * External definitions
 */
int
srv_constr( struct server *srv )
{
    size_t      conf_sz;    /* No. of elements in config struct */
    const char *dl_err;

    srv->running = 0;
    srv->socket = -1;
    srv->n_thr = 0;
    srv->mod_sz = 0;

    check( !cl_constr( &srv->innc, INNC_SZ ), "Could not initialize inncomming queue" );
    check( !cl_constr( &srv->queue, CONN_SZ ), "Could not initialize connection queue" );

    conf_sz = sizeof( config ) / sizeof( config[0] );
    srv->modules = malloc( conf_sz * sizeof( struct module ) );
    mem_check( srv->modules );

    dlerror();  /* Pop error */
    for ( ssize_t i=conf_sz-1 ; i>=0 ; --i ) {
        srv->modules[i].path = config[i].path;
        srv->modules[i].dl_handle = dlopen( config[i].filename, RTLD_NOW );
        check( srv->modules[i].dl_handle, "Could not open file ``%s'': ``%s''",
                config[i].filename, dlerror() );
        srv->modules[i].handle = dlsym( srv->modules[i].dl_handle, "handle" );
        dl_err = dlerror();
        check( srv->modules[i].handle && !dl_err, "Error opening module ``%s'': ``%s''",
                config[i].filename, dl_err );
        ++srv->mod_sz;
    }

    return 0;
error:
    return -1;

}


void
srv_destr( struct server *srv )
{
    void    *ignored;

    srv->running = 0;

    /*
    for ( size_t i=0 ; i<srv->n_thr ; ++i ) {
        pthread_cancel( srv->threads[i] );
    }
    */
    for ( size_t i=0 ; i<srv->n_thr ; ++i ) {
        pthread_join( srv->threads[i], &ignored );
    }
    srv->n_thr = 0;

    cl_destr( &srv->innc );
    cl_destr( &srv->queue );

    close( srv->socket );
    srv->socket = -1;

    while ( srv->mod_sz ) {
        --srv->mod_sz;
        dlclose( srv->modules[srv->mod_sz].dl_handle );
    }

}


int
srv_bind( struct server *srv, const char *port )
{
    if ( srv->socket >= 0 ) {
        close( srv->socket );
    }
    srv->socket = listen_port( port );
    check( srv->socket >= 0, "Could not bind to socket" );

    return 1;
error:
    return 0;
}


int
srv_start( struct server *srv )
{
    int         new_con;
    char        ip[INET6_ADDRSTRLEN];
    struct sockaddr_storage     addr;
    struct client   cli;
    socklen_t   addr_sz;

    srv->running = 1;
    /* Start worker threads */
    for ( size_t i=0 ; i<N_THREADS ; ++i ) {
        check( !pthread_create( &srv->threads[i], NULL, &srv_thread_fn, srv ),
                "Could not start worker thread" );
        ++srv->n_thr;
    }

    while( srv->running ) {
        addr_sz = sizeof( addr );
        new_con = accept( srv->socket, (struct sockaddr*)&addr, &addr_sz );
        if ( new_con < 0 ) {
            log_warn( "Failed connection" );
            continue;
        }

        get_name( ip, INET6_ADDRSTRLEN, &addr );
        debug( "<<<< New connection `%s' >>>>", ip );
        //serve_socket( new_con );
        if ( cli_constr( &cli, new_con, ip ) ) {
            log_warn( "Dropping inncomming connection because of allocation failure" );
            cli_destr( &cli );
        } else {
            if ( cl_push( &srv->innc, &cli ) ) {
                log_warn( "Dropping connection because of full queue" );
                cli_destr( &cli );
            }
        }

        //close( new_con );
    }

    return 0;
error:
    return -1;
}


void
srv_stop( struct server *srv )
{
    srv->running = 0;
}


void*
srv_thread_fn( void *vsrv )
{
    struct client   cli;
    struct server  *srv = vsrv;

    while ( srv->running ) {
        if ( !cl_shift( &srv->innc, &cli ) ) {
            // New connection
            if ( cli_recv_req( &cli ) ) {
                // Error eading request
                log_warn( "Dropping bad client" );
                cli_destr( &cli );
            } else {
                // Set handle and push to ready queue
                srv_handle( srv, &cli );
                if ( cl_push( &srv->queue, &cli ) ) {
                    log_warn( "Dropping connection because of full workqueue" );
                    cli_destr( &cli );
                }
            }
        } else if ( !cl_shift( &srv->queue, &cli ) ) {
            // Work on established connection
            if ( cli_work( &cli ) ) {
                // Not done yet
                if ( cl_push( &srv->queue, &cli ) ) {
                    log_warn( "Dropping connection because of full workqueue" );
                    cli_destr( &cli );
                }
            } else {
                // Done
                cli_destr( &cli );
            }
        } else {
            // Nothing to do. TODO: Wait for broadcast on new conn.
            sleep( 1 );
        }
    }
    debug( "Thread stoping" );
    return NULL;
}


/*
 * Static definitions
 */
int
listen_port( const char *port )
{
    struct addrinfo     hints;
    struct addrinfo    *res = NULL;
    struct addrinfo    *it;
    int                 status;
    int                 sock = -1;
    int                 yes;
    char                ipstr[INET6_ADDRSTRLEN];


    memset( &hints, 0, sizeof( hints ) );
    hints.ai_family = AF_INET; // TODO: AF_UNSPEC
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags  = AI_PASSIVE;

    status = getaddrinfo( NULL, port, &hints, &res );
    check( !status, "getaddrinfo: `%s'\n", gai_strerror( status ) );

    /* Loop until connection is made */
    for ( it = res; it; it = it->ai_next ) {
        sock = socket( it->ai_family, it->ai_socktype, it->ai_protocol );
        if ( sock < 0 ) {
            debug( "Could not create socket" );
            continue;
        }

        yes = 1;
        if ( setsockopt( sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int) ) < 0 ) {
            close( sock );
            sock = -1;
            debug( "setsockopt" );
            continue;
        }

        if ( bind( sock, it->ai_addr, it->ai_addrlen ) < 0 ) {
            close( sock );
            sock = -1;
            debug( "Could not bind" );
            continue;
        }

        break;
    }

    check( it, "No valid connection" );

    freeaddrinfo( res );
    res = NULL;

    check( !listen( sock, BACKLOG ), "Could not listen on socket" );

    return sock;

error :
    if ( res )
        freeaddrinfo( res );
    if ( sock > 0 )
        close( sock );
    return -1;
}


void
srv_handle( const struct server *srv, struct client *cli )
{
    for ( size_t i=srv->mod_sz-1 ; i ; --i ) {
        if ( strcmp( srv->modules[i].path, cli->req.path ) == 0 ) {
            cli->con.handle = srv->modules[i].handle;
            return;
        }
    }
    cli->con.handle = srv->modules[0].handle;
}


static inline
const char *
get_name( char *dst, size_t dst_len, struct sockaddr_storage *ss )
{
    void *addr;

    if ( ss->ss_family == AF_INET ) {
        addr = &(((struct sockaddr_in*)ss)->sin_addr);
    } else {
        addr = &(((struct sockaddr_in6*)ss)->sin6_addr);
    }

    return inet_ntop( ss->ss_family, addr, dst, dst_len );
}
