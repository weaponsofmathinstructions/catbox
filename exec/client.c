#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "client.h"
#include "edebug.h"
#include "request_priv.h"

int
cli_constr( struct client *c, int socket, const char *ip )
{
    memset( c, '\0', sizeof( struct client ) );

    check( !req_constr( &c->req ), "Could not initialize request" );

    c->con.socket = socket;
    c->con.req = &c->req;
    if ( ip ) {
        strcpy( c->con.ip, ip );
    }

    c->buff = malloc( 32 );
    mem_check( c->buff );
    memset( c->buff, 0, 32 );
    c->buff_sz = 32;

    return 0;
error:
    return -1;
}


void
cli_destr( struct client *c )
{
    close( c->con.socket );
    free( c->buff );
    req_destr( &c->req );
    memset( c, '\0', sizeof( struct client ) );
}


int
cli_recv_req( struct client *c )
{
    char           *tmp;
    int             socket = c->con.socket;
    size_t          endline;        /* Index of crlf */
    const size_t    min_cap = 64;   /* Min free space in buff before a recv */
    ssize_t         bytes_read;
    int             done = 0;

    req_constr( &c->req );

    while ( !done ) {
        /* Read one line */
        while ( strcspn( c->buff, "\n" ) == c->buff_end ) {
            if ( c->buff_sz - c->buff_end < min_cap ) {
                /* Double buffer size */
                tmp = realloc( c->buff, c->buff_sz<<1 );
                mem_check( tmp );
                c->buff = tmp;
                memset( c->buff+c->buff_sz, 0, c->buff_sz );
                c->buff_sz <<= 1;
            }

            bytes_read = recv( socket, c->buff+c->buff_end, c->buff_sz-1-c->buff_end, 0 );
            check( bytes_read > 0, "Could not read from socket" );
            c->buff_end += bytes_read;
            c->buff[c->buff_end] = '\0';

        }
        endline = strcspn( c->buff, "\n" ) + 1;
        done = req_addline( &c->req, c->buff, endline );
        // Remove line  from buff
        memmove( c->buff, c->buff+endline, c->buff_end-endline+1 );
        c->buff_end -= endline;
    }

    check( done > 0, "Bad request" );
    check( req_is_sane( &c->req ), "Invalid request" );

    return 0;
error:
    return -1;
}
