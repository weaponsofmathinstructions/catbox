#pragma once
#include <stddef.h>

#include "catbox.h"

struct client {
    struct request  req;
    struct con      con;
    char           *buff;       /* Recv buffer */
    size_t          buff_sz;    /* Buff size */
    size_t          buff_end;   /* Index of last char in buff */
};

/**
 * Constructor.
 * Clears the struct and sets socket fd.
 * @param socket    Socket fd. to recv from.
 * @param ip        Ip adsress.
 * @returns 0 on success, -1 on failure.
 */
int  cli_constr( struct client *c, int socket, const char *ip );
/**
 * Destructor.
 * Closes socket and frees used memory.
 */
void cli_destr( struct client *c );

/**
 * Reads a http request from socket.
 * @returns 0 on success, -1 on failure.
 */
int  cli_recv_req( struct client *c );

/**
 * Does one iteration of work.
 * @return returns 1 if there is more to do, 0 on completion.
 */
static inline int
cli_work( struct client *c )
{
    c->con.handle( &c->con );
    return c->con.handle ? 1 : 0;
}
