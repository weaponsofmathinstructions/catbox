#pragma once

#include <pthread.h>
#include <stddef.h>

#include "client_list.h"

#define BACKLOG     10      /* Pending que size */
#define N_THREADS    4      /* Worker threads */
#define INNC_SZ      5      /* Inncomming que size */
#define CONN_SZ     20      /* Max number of connections */


struct module {
    const char *path;
    con_handle  handle;
    void       *dl_handle;
};

struct server {
    int                 socket; /**< Socket to accept connections from */
    struct client_list  innc;   /**< Inncomming new connections */
    struct client_list  queue;  /**< Established connections */
    pthread_t           threads[N_THREADS];
    size_t              n_thr;  /**< Number of running threads */
    volatile int        running;
    struct module      *modules;
    size_t              mod_sz; /**< Numbers of loaded modules */
};

/**
 * Constructor.
 * @return returns 0 on success, -1 on failure.
 */
int  srv_constr( struct server * );
/**
 * Destructor.
 */
void srv_destr( struct server * );

/**
 * Binds to a port.
 * @param port port number (as string) or scheme to bind.
 */
int  srv_bind( struct server *, const char *port );
/**
 * Start server.
 * Creates worker threads and servers inncomming connections until
 * srv_stop is called.
 * @returns 0 on clean shutdown, -1 on startup error and 1 on error
 * shuting down.
 */
int  srv_start( struct server * );
/**
 * Stops server.
 */
void srv_stop( struct server * );

/**
 * Worker thread.
 */
void* srv_thread_fn( void * );

