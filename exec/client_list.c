#include <stdlib.h>

#include "client_list.h"
#include "edebug.h"

/* FIXME: Make this a ringbuffer */

int
cl_constr( struct client_list *cl, size_t max )
{
    size_t  sz = max * sizeof( struct client );

    cl->c = NULL;
    check( !pthread_mutex_init( &cl->mutex, NULL ), "Could not init mutex" );
    cl->c = malloc( sz );
    mem_check( cl->c );
    memset( cl->c, 0, sz );
    cl->max = max;
    cl->n = 0;

    return 0;
error:
    return -1;
}


void
cl_destr( struct client_list *cl )
{
    pthread_mutex_destroy( &cl->mutex );
    free( cl->c );
    memset( cl, 0, sizeof( struct client_list ) );
}


int
cl_push( struct client_list *cl, struct client *cli )
{
    cl_lock( cl );
    if ( cl->n >= cl->max ) {
        log_warn( "client_list rejecting client: full" );
        cl_unlock( cl );
        return -1;
    }

    memcpy( &cl->c[cl->n], cli, sizeof( struct client ) );
    ++cl->n;
    cl_unlock( cl );

    return 0;
}


int
cl_shift( struct client_list *cl, struct client *cli )
{
    cl_lock( cl );
    if ( cl->n == 0 ) {
        cl_unlock( cl );
        return -1;
    }

    memcpy( cli, &cl->c[0], sizeof( struct client ) );
    --cl->n;

    memmove( &cl->c[0], &cl->c[1], cl->n * sizeof( struct client ));
    cl_unlock( cl );

    /* Update pointer to new memory location */
    cli->con.req = &cli->req;

    return 0;
}
