#pragma once

#include "request.h"


int     req_constr( struct request * );
void    req_destr( struct request * );

/**
 * Adds a line to request.
 * @return Returns -1 on failure, 0 on success, 1 if parsing is done.
 */
int     req_addline( struct request *, const char *, size_t );
/**
 * Tests if the request is valid.
 * @returns 1 if valid, 0 if not.
 */
int     req_is_sane( const struct request * );
