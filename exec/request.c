#include <stdlib.h>

#include "edebug.h"
#include "request_priv.h"


/*
 * Static declarations.
 */
int
req_add_query( struct request *req, const char *qstr, size_t len );

/*
 * External definitions.
 */
int
req_constr( struct request *req )
{
    memset( req, 0, sizeof( struct request ) );
    req->headers = malloc( sizeof(char*) );
    mem_check( req->headers );
    req->headers[0] = NULL;
    req->headers_sz = 1;

    return 0;
error:
    return -1;
}


void
req_destr( struct request *req )
{
    if ( req->query ) {
        char **willy = req->query;
        while ( *willy ) {
            free( *willy );
            ++willy;
        }
        free( req->query );
    }
    free( req->version );
    for( size_t i=0 ; i<req->n_headers ; ++i ) {
        free( req->headers[i] );
    }
    free( req->headers );
    memset( req, 0, sizeof( struct request ) );
}


int
req_addline( struct request *req, const char *str, size_t len )
{

    check( len > 1, "Malformed request" );
    check( str[len-1]=='\n' && str[len-2]=='\r', "Malformed request" );

    if ( !req->path ) {
        /* First line */
        size_t  space;

        if ( len == 2 ) {
            // Ignore empty lines in the begining
            return 0;
        }

        if ( strncmp( str, "GET", 3 ) == 0 ) {
            req->type = E_GET;
        } else {
            // TODO: handle other requests than GET
            log_warn( "Refusing non GET request" );
            return -1;
        }
        // Eat request and whitespace
        space = strcspn( str, " \t" );
        check( space < len, "Malformed request" );
        space += strspn( str+space, " \t" );
        len -= space;
        str += space;
        // Query string is everything until first whitespace
        space = strcspn( str, " \t" );
        size_t split = strcspn( str, " \t?" );
        size_t p_sz = split > len-2 ? len-2 : split;
        req->path = malloc( p_sz+1 );
        mem_check( req->path );
        memcpy( req->path, str, p_sz );
        req->path[p_sz] = '\0';
        if ( split < space ) {
            size_t q_sz = space > len-2 ? len-2 : space;
            p_sz += 1; // Ignore leading '?'
            q_sz -= p_sz;
            check( !req_add_query( req, str+p_sz, q_sz ),
                    "Could not parse querystring" );
        }
        // Test if http version is specified
        if ( space < len ) {
            space += strspn( str+space, " \t" );
            len -= space;
            str += space;
            req->version = malloc( len - 1 );
            mem_check( req->version );
            memcpy( req->version, str, len-2 );
            req->version[len-2] = '\0';
        } else {
            req->version = malloc( 9 );
            mem_check( req->version );
            strcpy( req->version, "HTTP/1.0" );
            return 1;
        }

    } else {
        if ( len == 2 ) {
            /* Empty line, job done */
            return 1;
        }
        if ( req->n_headers == req->headers_sz ) {
            char **tmp;
            tmp = realloc( req->headers, (req->headers_sz<<2)*sizeof(char*) );
            mem_check( tmp );
            req->headers = tmp;
            req->headers_sz <<= 2;
        }
        /* Malloc and copy string without \r\n */
        req->headers[req->n_headers] = malloc( len-1 );
        mem_check( req->headers[req->n_headers] );
        memcpy( req->headers[req->n_headers], str, len-2 );
        req->headers[req->n_headers][len-2] = '\0';
        ++req->n_headers;
    }

    return 0;

error:
    return -1;
}


int
req_is_sane( const struct request *req )
{
    size_t  len;
    size_t  valid;

    if ( req->type == E_INVALID ) return 0;
    /* Test path */
    if ( !req->path ) return 0;
    if ( req->path[0] != '/' ) return 0;
    len = strlen( req->path );
    valid = strspn( req->path,  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz"
                                "0123456789-_.~%/" );
    if ( len != valid ) return 0;
    /* Test query string for invalid chars */
    if ( req->query ) {
        unsigned i = 0;
        while ( req->query[i] ) {
            len = strlen( req->query[i] );
            valid = strspn( req->query[i],
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz"
                                "0123456789-_.~%+" );
            if ( len != valid ) return 0;
            ++i;
            if ( !req->query[i] ) return 0;
            len = strlen( req->query[i] );
            valid = strspn( req->query[i],
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                "abcdefghijklmnopqrstuvwxyz"
                                "0123456789-_.~%?+*()" );
            if ( len != valid ) return 0;
            ++i;
        }
    }
    /* Test http version */
    if ( strcmp( req->version, "HTTP/1.0" ) != 0 &&
            strcmp( req->version, "HTTP/1.1" ) != 0 ) return 0;

    return 1;
}

/*
 * Static definitions.
 */
int
req_add_query( struct request *req, const char *qstr, size_t q_sz )
{
    unsigned    n;
    size_t      len;
    size_t      k_sz;   /* Length of key */

    if ( !qstr || q_sz==0 )
        return 0;

    /* Count n of params */
    n = len = 0;
    while ( len < q_sz ) {
        ++n;
        len += strcspn( qstr+len, "&" );
    }

    /* Allocate memory */
    req->query = malloc( (2*n+1)*sizeof( char* ) );
    mem_check( req->query );
    memset( req->query, 0, (2*n+1)*sizeof( char* ) );

    n = 0;
    while ( q_sz ) {
        /* Copy key */
        k_sz = strcspn( qstr, "=&" );
        k_sz = k_sz>q_sz? q_sz : k_sz;
        req->query[2*n] = malloc( k_sz + 1 );
        mem_check( req->query[2*n] );
        memcpy( req->query[2*n], qstr, k_sz );
        req->query[2*n][k_sz] = '\0';

        q_sz -= k_sz;
        qstr += k_sz;
        if ( *qstr == '=' ) {
            /* Value given, copy */
            ++qstr;
            --q_sz;
            len = strcspn( qstr, "&" );
            len = len>q_sz ? q_sz : len;
        } else {
            len = 0;
        }
        req->query[2*n+1] = malloc( len+1 );
        mem_check( req->query[2*n+1] );
        memcpy( req->query[2*n+1], qstr, len );
        req->query[2*n+1][len] = '\0';

        q_sz -= len;
        qstr += len;
        ++n;
    }
    //req->query[2*n] = NULL;

    return 0;
error:
    return -1;
}
