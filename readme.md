# Catbox #

## Overview ##
Catbox is a small proxy server intended to give Unity3d access to files
outside of it's sandbox.

It is currently in a early stage of development.

Catbox is very modular making use of dynamic loaded libraries. It uses
`dlopen` to open it's extentions and expects to find a function matching
the signature `void handle( struct con* )`.

### libcatbox ###
Contains the core functionality. The folder `include` contains the
public api that's supposed to be used.

### fileserv ###
Is the default request handler. It searches the folder `www` for files
and serves them.

### realy ###
Relays the url requested back to the client.

### makepng ###
Downloads the url requested, tests that it is a image and serves the
client the image converted to png format.


## Caveat ##
This program will happily relay any url it has access to, including
[http://internal.localhost/secret/file](http://internal.localhost/secret/file),
and could be a threat to your firewall/security settings.
