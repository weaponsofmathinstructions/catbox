#define _POSIX_C_SOURCE 200112L

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <unistd.h>

#include "catbox.h"
#include "edebug.h"

/*
 * Static declarations
 */
void sendimg( int to, const char *url );
void sendpng( int to, const char *buff, size_t len );

/*
 * External definitions
 */
void
handle( struct con *con )
{
    char       *str = NULL;
    const char *url;
    int         src_sock;
    size_t      len;


    if ( con->req->type != E_GET ) {
        e_501( con->socket ); /* Not yet implemented */
        con->handle = NULL;
        return;
    }
    url = req_q_val( con->req, "url" );
    if ( !url ) {
        /* No url given */
        e_400( con->socket ); /* Bad Request */
        con->handle = NULL;
        return;
    }
    len = strlen( url );
    str = malloc( len+1 );
    mem_check( str );
    unescape( str, url, len+1 );

    sendimg( con->socket, str );

error:
    free( str );
    con->handle = NULL;
}


/*
 * Static definitions
 */
void
sendimg( int to, const char *url )
{
    char       *buff = NULL;
    char       *tmp;
    size_t      buff_sz;
    size_t      len;
    size_t      endl;
    size_t      lineno;
    size_t      cont_sz;
    ssize_t     rcv;
    int         from = -1;  /* Socket fd */
    int         done = 0;
    struct url  u;

    const char *head = "GET ";
    const char *mid  = " HTTP/1.1\r\nHost: ";
    const char *tail = "\r\nConnection: close\r\n\r\n";

    /* Connect to host to dl image */
    url_constr( &u );
    check( !url_fromstr( &u, url ), "Could not parse url" );
    from = connect_host( &u );
    check( from > 0, "Could not connect to ``%s''", u.host );

    debug( "Sending request for ``%s''", url );
    /* Send request */
    len = strlen( head );
    check( send( from, head, len, 0 ) == len, "makepng: Connection closed" );
    len = strlen( u.path );
    check( send( from, u.path, len, 0 ) == len, "makepng: Connection closed" );
    if ( u.query ) {
        check( send( from, "?", 1, 0 ) == 1, "makepng: Connection closed" );
        len = strlen( u.query );
        check( send( from, u.query, len, 0 ) == len, "makepng: Connection closed" );
    }
    len = strlen( mid );
    check( send( from, mid, len, 0 ) == len, "makepng: Connection closed" );
    len = strlen( u.host );
    check( send( from, u.host, len, 0 ) == len, "makepng: Connection closed" );
    len = strlen( tail );
    check( send( from, tail, len, 0 ) == len, "makepng: Connection closed" );

    /* Recv responce */
    buff_sz = 32;   /* Random round number */
    buff = malloc( buff_sz );
    memset( buff, '\0', buff_sz );
    mem_check( buff );
    cont_sz = len = lineno = 0;
    while ( !done ) {
        /* Recv whole header */
        while ( (endl=strcspn( buff, "\n" )) == len ) {
            /* Recv one line */
            if ( buff_sz - len < 32 ) {
                /* Increase buffer size */
                check( buff_sz < 256, "Missbehaving remote server is trying to feed us to long lines" );
                tmp = realloc( buff, buff_sz<<1 );
                mem_check( tmp );
                buff = tmp;
                memset( buff+buff_sz, '\0', buff_sz );
                buff_sz <<=1;
            }
            rcv = recv( from, buff+len, buff_sz-len-1, 0 );
            check( rcv>0, "Connection closed" );
            len += rcv;
        }
        if ( endl < 2 )
            /* Empty line, done */
            done = 1;
        else {
            if ( lineno == 0 ) {
                /* First line, expecting "HTTP/1.1 200 OK" */
                check( endl >= 12, "To short first line in response from remote server" );
                check( strncmp( buff, "HTTP/1.", 7 )==0, "Invalid responce from remote server" );
                check( buff[9] == '2', "Remote server did not respond with code 200" );
            } else if ( strncmp( buff, "Content-Length:", 15 ) == 0 ) {
                /* Content-Length: [size] */
                check( endl > 15, "Remote server sent broken header" );
                cont_sz = atoi( buff + 15 );
            } else if ( strncmp( buff, "Content-Type:", 13 ) == 0 ) {
                /* Content-Type: [mimetype] */
                check( endl > 13, "Remote server sent broken header" );
                check( strncmp( "image/", buff+13+strspn(buff+13, " \t"), 6 ) == 0,
                        "makepng: Requested url is not an image" );
                // TODO: skip conversion if it is a image/png
            }

        }
        ++endl;
        memmove( buff, buff+endl, len-endl+1 );
        len -= endl;
        ++lineno;
    }

    /* Requested url is now verified to exist and be of type image/url */
    buff_sz = cont_sz;
    tmp = realloc( buff, buff_sz );
    mem_check( tmp );
    buff = tmp;
    while ( len < buff_sz ) {
        /* Load the whole image to buffer */
        rcv = recv( from, buff+len, buff_sz-len, 0 );
        check( rcv>0, "Connection closed" );
        len += rcv;
    }
    close( from );  /* Not needed anymore */
    from = -1;

    sendpng( to, buff, buff_sz );


    free( buff );
    url_destr( &u );
    return;

error:
    e_500( to );
    free( buff );
    url_destr( &u );
    if ( from > 0 ) {
        close( from );
    }
}

void sendpng( int to, const char *buff, size_t len )
{
#   if 0
    FILE       *img = NULL;
    int         png = -1;
    char       *png_map = MAP_FAILED;
    off_t       png_sz;

    /* TODO
     * Create a unique filename based on hash/time etc. and remove file
     * at the end of the function.
     */
    debug( "Writing to temp file. Size: %lu", len );
    /* Write buff to `temp' file */
    img = fopen( "temp", "w" );
    check( img, "makepng: Could not create temp file" );
    check( fwrite( buff, 1, len, img ) == len,
            "makepng: Could not write to temp file" );
    fflush( img );

    debug( "Converting" );
    /* Convert `temp' file to `temp.png' */
    check( !system( "convert temp temp.png" ),
            "makepng: Could not convert image to png" );

    debug( "Reading png file" );
    /* Read `temp.png' */
    png = open( "temp.png", O_RDONLY, 0 );
    check( png >= 0, "makepng: Could not open temp.png file" );

    /* Find file size */
    png_sz = lseek( png, 0, SEEK_END );
    check( png_sz>0, "makepng: Could not read from tepm.png" );
    lseek( png, 0, SEEK_SET );
    /* Mmap to memory */
    png_map = mmap( NULL, png_sz, PROT_READ, MAP_SHARED, png, 0 );
    check( png_map != MAP_FAILED, "makepng: Could not map file to memory" );

    /* Send responce to client */
    send_response( to, 200, "OK", "image/png", png_map, png_sz );

    fclose( img );
    munmap( png_map, png_sz );
    close( png );

    return;

error:
    e_500( to );
    if ( img )
        fclose( img );
    if ( png_map != MAP_FAILED )
        munmap( png_map, png_sz );
    if ( png >= 0 )
        close( png );
#   endif /* 0 */
#   define WRITE    1
#   define READ     0
    int     in[2] = { -1, -1 };
    int     out[2] = { -1, -1 };
    pid_t   pid;
    char   *command[] = { "convert", "-", "png:-", NULL };
    char   *png_buff;
    char   *tmp;
    size_t  img_end;
    size_t  png_buff_sz;
    size_t  png_buff_end;
    ssize_t diff;

    check( !pipe( in ), "makepng: Could not set up pipes" );
    check( !pipe( out ), "makepng: Could not set up pipes" );

    switch ( pid = fork() ) {
    case -1 :
        check( 0, "makepng: fork failed" );
        break;
    case 0 :
        /* Child */
        // Close unused pipes
        close( in[WRITE] );
        close( out[READ] );
        // Redirect stdin and stdout
        close( fileno( stdin ) );
        fcntl( in[READ], F_DUPFD, fileno( stdin ) );
        close( fileno( stdout ) );
        fcntl( out[WRITE], F_DUPFD, fileno( stdout ) );
        // Exec convert command
        execvp( command[0], command );
        // Should not be reached
        fprintf( stderr, "makepng: exec failed\n" );
        exit(0);
        break;
    default :
        // Close unused pipes
        close( in[READ] );
        close( out[WRITE] );
        in[READ] = out[WRITE] = -1;
        break;
    }

    png_buff_end = 0;
    png_buff_sz = 256;
    png_buff = malloc( png_buff_sz );
    mem_check( png_buff );

    /* Set out[READ] to non-blocking */
    fcntl( out[READ], F_SETFL, fcntl( out[READ], F_GETFL) | O_NONBLOCK );
    fcntl( in[WRITE], F_SETFL, fcntl( in[WRITE], F_GETFL) | O_NONBLOCK );

    /* Write all of buff to convert */
    img_end = 0;
    while ( img_end < len ) {
        /* Poll if anything is writen to stdout */
        if ( png_buff_sz - png_buff_end < 64 ) {
            /* Allocate more space for png_buff */
            tmp = realloc( png_buff, png_buff_sz<<1 );
            mem_check( tmp );
            png_buff = tmp;
            png_buff_sz <<= 1;
        }
        diff = read( out[READ], png_buff+png_buff_end, png_buff_sz-png_buff_end );
        if ( diff < 0 ) {
            check( errno == EAGAIN, "makepng: Error converting file" );
        } else {
            check( diff, "makepng: Error converting file" );
            png_buff_end += diff;
        }

        /* Write to stdin */
        diff = write( in[WRITE], buff+img_end, len-img_end );
        if ( diff < 0 ) {
            check( errno == EAGAIN, "makepng: Error converting file" );
        } else {
            check( diff>0, "makepng: Error converting file" );
            img_end += diff;
        }
    }

    close( in[WRITE] );
    in[WRITE] = -1;

    /* Now set out[READ] to blocking again and read everything */
    fcntl( out[READ], F_SETFL, fcntl( out[READ], F_GETFL) & ~O_NONBLOCK );
    do {
        /* Poll if anything is writen to stdout */
        if ( png_buff_sz - png_buff_end < 64 ) {
            /* Allocate more space for png_buff */
            tmp = realloc( png_buff, png_buff_sz<<1 );
            mem_check( tmp );
            png_buff = tmp;
            png_buff_sz <<= 1;
        }
        diff = read( out[READ], png_buff+png_buff_end, png_buff_sz-png_buff_end );
        if ( diff < 0 ) {
            check( errno == EAGAIN, "makepng: Error converting file" );
        } else {
            png_buff_end += diff;
        }
    } while ( diff );

    /* Close unused pipes */
    close( out[READ] );
    in[WRITE] = out[READ] = -1;

    send_response( to, 200, "OK", "image/png", png_buff, png_buff_end );
    free( png_buff );
    return;

error:
    e_500( to );
    if ( in[0] >= 0 )
        close( in[0] );
    if ( in[1] >= 0 )
        close( in[1] );
    if ( out[0] >= 0 )
        close( out[0] );
    if ( out[1] >= 0 )
        close( out[1] );
    free( png_buff );
#   undef WRITE
#   undef READ
}
