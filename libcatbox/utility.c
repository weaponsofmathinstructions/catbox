/* getaddrinfo and friends are not part of c99 */
#define _POSIX_C_SOURCE 200112L

#include <netdb.h>
#include <netinet/in.h>
#include <stddef.h>
#include <sys/socket.h>
#include <string.h>
#include <unistd.h>

#include "edebug.h"
#include "utility_priv.h"

/*
 * Static declarations
 */
static inline char
asciify( const char* );

static inline char
hexval( char );

struct mime_type {
    size_t      len;
    const char *ext;
    const char *mime_type;
};

/*
 * External definitions
 */
size_t
unescape( char *dest, const char *src, size_t len )
{
    size_t      r;
    size_t      w;

    r = w = 0;
    while ( w < len-1 && src[r] ) {
        if ( src[r] != '%' ) {
            dest[w] = src[r];
            ++r;
        } else {
            dest[w] = asciify( src+r );
            r += 3;
        }
        ++w;
    }
    dest[w] = '\0';

    return w;
}


const char *
mime_type( const char *filename, size_t len )
{
    struct mime_type    types[] = {
        { 5, ".html",   "text/html" },
        { 4, ".xml",    "application/xml" },
        { 4, ".txt",    "text/plain" },
        { 4, ".png",    "image/png" },
        { 4, ".jpg",    "image/jpg" },
        { 4, ".gif",    "image/gif" },
        //TODO: fill
    };
    const int n = 6;    // size of types

    for ( int i=0 ; i<n ; ++i ) {
        if ( len > types[i].len &&
                strncmp( filename+len-types[i].len, types[i].ext, types[i].len ) == 0 ) {
            return types[i].mime_type;
        }
    }

    return "application/octet-stream";
}


int
send_head( int sock, int scode, const char *rphr, const char *type, size_t body_sz )
{
#   define  BUFFLEN     64
    char    buff[BUFFLEN];
    ssize_t len;

    /* "HTTP/1.1 [scode] OK" */
    check( scode>99 && scode<600, "Invalid status code %d", scode );
    len = snprintf( buff, BUFFLEN, "HTTP/1.1 %d %s\r\n", scode, rphr );
    check( len>0 && len<BUFFLEN, "Error creating http response" );
    check( send( sock, buff, len, 0 ) == len, "Could not write to socket" );

    /* "Content-Type: [type]" */
    len = snprintf( buff, BUFFLEN, "Content-Type: %s\r\n", type );
    check( len>0 && len<BUFFLEN, "Error creating http response" );
    check( send( sock, buff, len, 0 ) == len, "Could not write to socket" );

    /* "Content-Length: [body_sz]" */
    len = snprintf( buff, BUFFLEN, "Content-Length: %lu\r\n", body_sz );
    check( len>0 && len<BUFFLEN, "Error creating http response" );
    check( send( sock, buff, len, 0 ) == len, "Could not write to socket" );

    /* "Connection: close" + "" */
    len = snprintf( buff, BUFFLEN, "Connection: close\r\n\r\n" );
    check( len>0 && len<BUFFLEN, "Error creating http response" );
    check( send( sock, buff, len, 0 ) == len, "Could not write to socket" );


    return 0;
error:
    return -1;
#   undef   BUFFLEN
}

int
send_response( int sock, int scode, const char *rphr, const char *type, const char *body, size_t body_sz )
{
    /* Write header first */
    check( !send_head( sock, scode, rphr, type, body_sz ), "Could not write to socket" );
    /* Write body. TODO: write in a non blocking way? */
    check( send( sock, body, body_sz, 0 ) == body_sz, "Could not write to socket" );

    return 0;
error:
    return -1;
}


int
connect_host( const struct url *url )
{
    struct addrinfo     hints;
    struct addrinfo    *res = NULL;
    struct addrinfo    *it;
    int                 status;
    int                 sock = -1;
    char                ipstr[INET6_ADDRSTRLEN];


    memset( &hints, 0, sizeof( hints ) );
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    status = getaddrinfo( url->host, url->port ? url->port : url->scheme, &hints, &res );
    check( !status, "getaddrinfo: `%s'\n", gai_strerror( status ) );

    /* Loop until connection is made */
    for ( it = res; it; it = it->ai_next ) {
        sock = socket( it->ai_family, it->ai_socktype, it->ai_protocol );
        if ( sock < 0 ) {
            debug( "Could not create socket" );
            continue;
        }

        if ( connect( sock, it->ai_addr, it->ai_addrlen ) ) {
            close( sock );
            sock = -1;
            debug( "Could not connect" );
            continue;
        }

        break;
    }

    check( it, "No valid connection" );

    freeaddrinfo( res );
    res = NULL;

    return sock;

error :
    if ( res )
        freeaddrinfo( res );
    if ( sock > 0 )
        close( sock );
    return -1;
}


/*
 * Static definitions
 */
static inline char
asciify( const char *str )
{
    if ( !( str[0] == '%' && str[1] && str[2] ) ) {
        return '\0';
    }

    return ( hexval( str[1] ) << 4 ) + hexval( str[2] );

}


static inline char
hexval( char c )
{
    if ( c >= 'a' && c <= 'f' )
        return c - 'a' + 10;
    else if ( c >= 'A' && c <= 'F' )
        return c - 'A' + 10;
    else if ( c >= '0' && c <= '9' )
        return c - '0';
    else
        return 0;
}

