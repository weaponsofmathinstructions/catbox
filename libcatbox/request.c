#include <string.h>

#include "request.h"


const char *
req_q_val( const struct request *req, const char *key )
{
    unsigned i = 0;

    if ( !req->query ) return NULL;

    while ( req->query[i] ) {
        if ( strcmp( req->query[i], key ) == 0 )
            return req->query[i+1];
        i += 1;
    }

    return NULL;
}
