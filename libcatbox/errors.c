#include <string.h>

#include "edebug.h"
#include "errors.h"
#include "utility.h"

void
e_400( int socket )
{
    size_t      len;
    const char *html =
        "<!DOCTYPE html>\n"
        "<html>\n"
        "<body>\n"
        "    <title>Bad Request</title>\n"
        "    <h1>Error 400 (Bad Request)</h1>\n"
        "    <p>Server cannot understan your request. Assuming this is your fault.</p>\n"
        "</body>\n"
        "</html>\n";
    len = strlen( html );

    check( !send_response( socket, 400, "Bad Request", "text/html; charset=UTF-8", html, len ),
            "Could not send 400" );

    /* Falltorugh */
error:
    return;
}

void
e_404( int socket )
{
    size_t      len;
    const char *html =
        "<!DOCTYPE html>\n"
        "<html>\n"
        "<body>\n"
        "    <title>Not found</title>\n"
        "    <h1>Error 404 (Not Found)</h1>\n"
        "    <p>The url was not found on this server.</p>\n"
        "</body>\n"
        "</html>\n";
    len = strlen( html );

    check( !send_response( socket, 404, "Not Found", "text/html; charset=UTF-8", html, len ),
            "Could not send 404" );

    /* Falltorugh */
error:
    return;
}


void e_500( int socket )
{
    size_t      len;
    const char *html =
        "<!DOCTYPE html>\n"
        "<html>\n"
        "<body>\n"
        "    <title>Internal Server Error</title>\n"
        "    <h1>Error 500 (Internal Server Error)</h1>\n"
        "    <p>Something bad has happened.</p>\n"
        "</body>\n"
        "</html>\n";
    len = strlen( html );

    check( !send_response( socket, 500, "Internal Server Error", "text/html; charset=UTF-8", html, len ),
            "Could not send 500" );

    /* Falltorugh */
error:
    return;
}


void e_501( int socket )
{
    size_t      len;
    const char *html =
        "<!DOCTYPE html>\n"
        "<html>\n"
        "<body>\n"
        "    <title>Not Implemented</title>\n"
        "    <h1>Error 501 (Not Implemented)</h1>\n"
        "    <p>Method not yet implemented. Please try again another year.</p>\n"
        "</body>\n"
        "</html>\n";
    len = strlen( html );

    check( !send_response( socket, 501, "Not Implemented", "text/html; charset=UTF-8", html, len ),
            "Could not send 501" );

    /* Falltorugh */
error:
    return;
}
