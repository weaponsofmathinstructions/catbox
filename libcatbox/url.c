#include <ctype.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "edebug.h"
#include "utility.h"
#include "url_priv.h"

/* Returns 1 if string has unescaped characters */
static inline int
has_unescaped( const char * );
/* Returns 1 if string is a valid path */
static inline int
is_path( const char * );
/* Returns 1 if string is a valid query string */
static inline int
is_query( const char * );
/* Returns 1 if string is number */
static inline int
is_number( const char * );


void
url_constr( struct url *u )
{
    memset( u, 0, sizeof( struct url ) );
}

void
url_destr( struct url *u )
{
    free( u->scheme );
    free( u->host );
    free( u->port );
    free( u->path );
    free( u->query );
    memset( u, 0, sizeof( struct url ) );
}

int
url_fromstr( struct url *u, const char *str )
{
    /* Abandon hope all who enters.
     * This function wil atemtpt to parse strings with no usage of
     * regexp or anything else that would make sense :)
     * TODO: use strspn instead strchr
     */
    const char     *p;
    const char     *host;
    const char     *port = NULL;
    const char     *path;
    const char     *query = NULL;
    size_t          scheme_l;
    size_t          host_l;
    size_t          port_l;
    size_t          path_l;
    size_t          query_l;
    size_t          len;

    len = strlen( str );

    /* Parse scheme */
    p = strchr( str, ':' );
    check( p, "Missing scheme" );

    scheme_l = p - str;
    check( scheme_l < len - 2, "Invalid url" );
    check( p[1] == '/' && p[2] == '/', "Invalid url" );

    host = p + 3;

    path = strchr( host, '/' );
    if ( !path ) {
        /* No path, insert root and assume the whole string is hostname */
        path = "/";
        path_l = 1;
        host_l = len - (host-str);
    } else {
        host_l = path - host;
        query = strchr( path, '?' );
        if ( !query ) {
            /* No querystring */
            path_l = len - (path-str);
        } else {
            path_l = query - path;
            ++query;
            query_l = len - (query - str);
        }
    }

    /* Test if hostname contains portnr */
    port = strchr( host, ':' );
    if ( port && port < path ) {
        /* ':' found before '/' */
        host_l = port-host;
        ++port;
        port_l = path-port;
    } else {
        port = NULL;
    }

    /* Copy strings */
    u->scheme = malloc( scheme_l+1 );
    u->host = malloc( host_l+1 );
    u->path = malloc( path_l+1 );
    if ( port ) {
        u->port = malloc( port_l+1 );
        mem_check( u->port );
        memcpy( u->port, port, port_l );
        u->port[port_l] = '\0';
    }
    if ( query ) {
        u->query = malloc( query_l+1 );
        mem_check( u->query );
        memcpy( u->query, query, query_l );
        u->query[query_l] = '\0';
    }
    mem_check( u->scheme );
    mem_check( u->host );
    mem_check( u->path );
    memcpy( u->scheme, str, scheme_l );
    u->scheme[scheme_l] = '\0';
    memcpy( u->host, host, host_l );
    u->host[host_l] = '\0';
    memcpy( u->path, path, path_l );
    u->path[path_l] = '\0';


    /*
    debug( "scheme: %s", u->scheme );
    debug( "host:   %s", u->host );
    debug( "port:   %s", u->port );
    debug( "path:   %s", u->path );
    debug( "query:  %s", u->query );
    */

    /* Final sanity test */
    check( !has_unescaped( u->scheme ), "Malformed url" );
    check( !has_unescaped( u->host ), "Malformed url" );
    if ( u->port )
        check( is_number( u->port ), "Malformed url" );
    check( is_path( u->path ), "Malformed url" );
    if ( u->query )
        check( is_query( u->query ), "Malformed url" );

    return 0;

error:
    url_destr( u );
    return -1;
}


static inline int
has_unescaped( const char *str )
{
    for ( ; *str ; ++str ) {
        if ( *str >= 'a' && *str <= 'z' )
            continue;
        if ( *str >= 'A' && *str <= 'Z' )
            continue;
        if ( *str >= '0' && *str <= '9' )
            continue;
        switch( *str ) {
        case '-':
        case '_':
        case '.':
        case '~':
            break;
        default :
            return 1;
        }
    }
    return 0;
}

static inline int
is_path( const char *str )
{
    for ( ; *str ; ++str ) {
        if ( *str >= 'a' && *str <= 'z' )
            continue;
        if ( *str >= 'A' && *str <= 'Z' )
            continue;
        if ( *str >= '0' && *str <= '9' )
            continue;
        switch( *str ) {
        case '-':
        case '_':
        case '.':
        case '~':
        case '/':
        case '%':
            break;
        default :
            return 0;
        }
    }
    return 1;
}

static inline int
is_query( const char *str )
{
    for ( ; *str ; ++str ) {
        if ( *str >= 'a' && *str <= 'z' )
            continue;
        if ( *str >= 'A' && *str <= 'Z' )
            continue;
        if ( *str >= '0' && *str <= '9' )
            continue;
        switch( *str ) {
        case '-':
        case '_':
        case '.':
        case '~':
        case '/':
        case '%':
        case '=':
        case ',':
        case '&':
        case '+':
        case '(':
        case ')':
        case '*':
            break;
        default :
            return 0;
        }
    }
    return 1;
}

static inline int
is_number( const char *str )
{
    while ( *str ) {
        if ( ! isdigit( *str ) )
            return 0;
        ++str;
    }
    return 1;
}
