#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#include "catbox.h"
#include "edebug.h"
#include "fileserv.h"

void
handle( struct con *con )
{
    int         file_d = -1;
    char       *str = NULL;
    char       *file_cont = MAP_FAILED;
    const char *file_type;
    size_t      len;
    off_t       map_sz;

    check( con->req->type == E_GET, "Cowardly refusing request not of type GET" );
    len = strlen( con->req->path );

    if ( len == 1 ) {
        /* Root domain, serve index.html */
        str = malloc( 15 );
        mem_check( str );
        snprintf( str, 15, "www/index.html" );
        file_type = "text/html";
    } else {
        str = malloc( len + 4 );
        mem_check( str );
        snprintf( str, len+4, "www%s", con->req->path );
        file_type = mime_type( con->req->path, len );
    }

    // Open file
    file_d = open( str, O_RDONLY, 0 );
    if ( file_d<0 ) {
        log_warn( "Could not open file `%s'", str );
        free( str );
        e_404( con->socket );
        con->handle = NULL;
        return;
    }
    // Find file size
    map_sz = lseek( file_d, 0, SEEK_END );
    check( map_sz>0, "Could not read file `%s'", str );
    lseek( file_d, 0, SEEK_SET );
    // mmap file to memory.
    file_cont = mmap( NULL, map_sz, PROT_READ, MAP_SHARED, file_d, 0 );
    check( file_cont != MAP_FAILED, "Could not read file `%s'", str );

    check( !send_response( con->socket, 200, "OK", file_type, file_cont, map_sz ),
            "Could not send response" );

    munmap( file_cont, map_sz );
    file_cont = MAP_FAILED;
    close( file_d );
    file_d = -1;
    free( str );
    str = NULL;
    con->handle = NULL;

    return;
error:
    if ( file_cont != MAP_FAILED )
        munmap( file_cont, map_sz );
    if ( file_d >= 0 )
        close( file_d );
    free( str );
    con->handle = NULL;
    return;
}
