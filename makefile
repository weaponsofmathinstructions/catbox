LIBS = libcatbox.so relay.so fileserv.so makepng.so
MAKE ?= make

project = catbox

all : $(project)

libcatbox.so : libcatbox/*
	$(MAKE) -C libcatbox
	ln libcatbox/libcatbox.so libcatbox.so

relay.so : relay/*
	$(MAKE) -C relay
	ln relay/relay.so relay.so

fileserv.so : fileserv/*
	$(MAKE) -C fileserv
	ln fileserv/fileserv.so fileserv.so

makepng.so : makepng/*
	$(MAKE) -C makepng
	ln makepng/makepng.so makepng.so

$(project) : exec/* $(LIBS)
	$(MAKE) -C exec
	ln exec/catbox catbox

clean :
	rm $(project) $(LIBS) || true
	$(MAKE) -C exec clean
	$(MAKE) -C libcatbox clean
	$(MAKE) -C relay clean
	$(MAKE) -C fileserv clean
	$(MAKE) -C makepng clean
