#pragma once

#include <errno.h>
#include <stdio.h>
#include <string.h>

#define err_str()   ( errno == 0 ? "None" : strerror( errno ) )

#ifdef NDEBUG
/* Somewhat quiet error logging */
#   define  debug( FMT, ... )
#   define  log_err( FMT, ... )   fprintf( stderr, "[error] errno:`%s' " FMT "\n", \
        err_str(), ##__VA_ARGS__ )
#   define  log_warn( FMT, ... )   printf( "[warn] errno:`%s' " FMT "\n", \
        err_str(), ##__VA_ARGS__ )
#   define  log_info( FMT, ... )   printf( "[info] " FMT "\n", \
        ##__VA_ARGS__ )
#else
/* Include file:lineno in error logging */
#   define  debug( FMT, ... )   printf( "[debug] %s:%d " FMT "\n", \
        __FILE__, __LINE__, ##__VA_ARGS__ )
#   define  log_err( FMT, ... )   fprintf( stderr, "[error] %s:%d errno:`%s' " FMT "\n", \
        __FILE__, __LINE__, err_str(), ##__VA_ARGS__ )
#   define  log_warn( FMT, ... )   printf( "[warn] %s:%d errno:`%s' " FMT "\n", \
        __FILE__, __LINE__, err_str(), ##__VA_ARGS__ )
#   define  log_info( FMT, ... )   printf( "[info] %s:%d " FMT "\n", \
        __FILE__, __LINE__, ##__VA_ARGS__ )
#endif

#define check( COND, FMT, ...)  if ( !(COND) ) { log_err( FMT, ##__VA_ARGS__ ); \
        errno = 0; goto error; }

#define mem_check( MEM )    check( MEM, "Out of memory." )
