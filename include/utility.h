#pragma once

#include <stddef.h>
#include "url.h"

/**
 * Unescapes percent encoding in a string.
 * Writes at most len bytes to dest, including terminating '\0'
 * If the src string is to long dest gets cut of, '\0' is always written.
 * Length of unescaped string is never longer than the escaped one,
 * making strlen( src ) + 1 a safe length for dest.
 * @param dest  string to write to.
 * @param src   Percent encoded string to unescape.
 * @param len   Max length of dest.
 * @return      strlen of dest.
 */
size_t
unescape( char *dest, const char *src, size_t len );

/**
 * Returns mime type of filename by extention.
 * Defaults to "application/octet-stream"
 * @param filename  Filename.
 * @param len       Length of filename.
 * @return          Mime-type.
 */
const char *
mime_type( const char *filename, size_t len );

/**
 * Sends the head of a http response to given port.
 * @param sock  Socket to write response to.
 * @param scode Status code.
 * @param rphr  Reason phrase.
 * @param type  Content type.
 * @param len   Message body length.
 * @return      0 on success. -1 on failure.
 */
int
send_head( int sock, int scode, const char *rphr, const char *type, size_t len );

/**
 * Sends a http response to given port.
 * @param sock  Socket to write response to.
 * @param scode Status code.
 * @param rphr  Reason phrase.
 * @param type  Content type.
 * @param body  Message body.
 * @param len   Message body length.
 * @return      0 on success. -1 on failure.
 */
int
send_response( int sock, int scode, const char *rphr, const char *type, const char *body, size_t len );

/**
 * Connects to a host by url.
 * @return returns socket fd on success, -1 on error.
 */
int connect_host( const struct url *url );
