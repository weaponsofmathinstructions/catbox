#pragma once

void e_400( int socket );   /* Bad Request */
void e_404( int socket );   /* Not Found */
void e_500( int socket );   /* Internal Server Error */
void e_501( int socket );   /* Not Implemented */
