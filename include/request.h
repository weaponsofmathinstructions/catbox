#pragma once
#include <stddef.h>

enum {
    E_INVALID = 0,
    E_GET,
    E_HEAD,
    E_POST,
    E_PUT,
    E_DELETE,
    E_TRACE,
    E_OPTIONS,
    E_CONNECT,
    E_PATCH
} request_type;


struct request {
    int         type;       /**< Type of request */
    char       *path;       /**< Path string */
    char      **query;      /**< Query string. NULL terminated array alternating
                                    key, value. If the key has no value, it is
                                    followed by a empty string. */
    char      **headers;    /**< NULL terminated array of header-lines */
    size_t      n_headers;  /**< No. of strings in headers */
    size_t      headers_sz; /**< Capacity in headers */
    char       *version;    /**< Http version */
};

/**
 * Finds the value of a querykey.
 * @return The value of key, NULL if not present.
 */
const char *
req_q_val( const struct request *req, const char *key );
