#pragma once

#include "errors.h"
#include "request.h"
#include "url.h"
#include "utility.h"

struct con;
typedef void (*con_handle)(struct con *);

/**
 * Information about a inncomming connection.
 */
struct con {
    char                    ip[46]; /**< Ip addr as sting */
    int                     socket; /**< Connection socket */
    const struct request   *req;    /**< Request made by client */
    con_handle              handle;
    void                   *userdata;
};

