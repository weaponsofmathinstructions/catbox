#pragma once

struct url {
    char   *scheme;
    char   *host;
    char   *port;
    char   *path;
    char   *query;
};


void    url_constr( struct url * );
void    url_destr( struct url * );
int     url_fromstr( struct url *, const char * );
