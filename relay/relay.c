#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "catbox.h"
#include "edebug.h"
#include "relay.h"

#define BUFFSZ      128

void
handle( struct con *con )
{
    char       *str = NULL;
    const char *url;
    size_t      len;

    if ( con->req->type != E_GET ) {
        e_501( con->socket ); /* Not yet implemented */
        con->handle = NULL;
        return;
    }
    url = req_q_val( con->req, "url" );
    if ( !url ) {
        /* No url given */
        e_400( con->socket ); /* Bad Request */
        con->handle = NULL;
        return;
    }
    len = strlen( url );
    str = malloc( len+1 );
    mem_check( str );
    unescape( str, url, len+1 );
    relay( con->socket, str );

error:
    free( str );
    con->handle = NULL;
}

int
relay( int write, const char *str )
{
    int         sock;
    int         res;
    char        buff[BUFFSZ];
    size_t      len;
    size_t      done;
    struct url  u;

    url_constr( &u );
    check( !url_fromstr( &u, str ), "Could not parse url" );

    sock = connect_host( &u );
    check( sock > 0, "Could not connect to %s\n", u.host );

    const char * head = "GET ";
    const char * mid  = " HTTP/1.1\r\nHost: ";
    const char * tail = "\r\nConnection: close\r\n\r\n";

    len = strlen( head );
    check( send( sock, head, len, 0 ) == len, "Connection closed" );
    len = strlen( u.path );
    check( send( sock, u.path, len, 0 ) == len, "Connection closed" );
    if ( u.query ) {
        check( send( sock, "?", 1, 0 ) == 1, "Connection closed" );
        len = strlen( u.query );
        check( send( sock, u.query, len, 0 ) == len, "Connection closed" );
    }
    len = strlen( mid );
    check( send( sock, mid, len, 0 ) == len, "Connection closed" );
    len = strlen( u.host );
    check( send( sock, u.host, len, 0 ) == len, "Connection closed" );
    len = strlen( tail );
    check( send( sock, tail, len, 0 ) == len, "Connection closed" );

    done = 0;
    while ( !done ) {
        res = read( sock, buff, BUFFSZ-1 );
        if ( res < 0 ) {
            check( errno == EAGAIN, "Could not read from socket" );
            continue;
        } else if ( res == 0 ) {
            done = 1;
        } else {
            check( send( write, buff, res, 0 ) == res, "Connection closed" );
        }
    }

    url_destr( &u );
    close( sock );

    debug( "Relayed ``%s''", str );
    return 0;
error :
    if ( sock > 0 )
        close( sock );
    url_destr( &u );
    return -1;
}
