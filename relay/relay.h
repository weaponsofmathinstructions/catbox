#pragma once

/**
 * Exported con_handle function.
 */
void handle( struct con * );

/**
 * Relays the content from an url to a given socket.
 * @param sock  socket to write to
 * @param str   url to read from
 * @return      0 on success, nonzero on failure
 */
int relay( int sock, const char *str );
